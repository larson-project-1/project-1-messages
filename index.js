import { Datastore } from '@google-cloud/datastore';
import express, { json } from 'express';
import cors from 'cors';
import axios from 'axios';

const app = express();
const datastore = new Datastore();

const loginURL = "https://login-dot-project-1-revature-larson.ue.r.appspot.com"

app.use(cors());
app.use(json());

const PORT = process.env.PORT || 1200;

app.listen(PORT, ()=>{
    console.log(`Listening on port ${PORT}`);
});

app.get("/", (req, res) => {
    res.send("Messages Service");
});

app.get("/messages", async(req, res) => {
    const query = datastore.createQuery('Message');
    if (req.query.recipient)query.filter('recipient', '=', req.query.recipient);
    if (req.query.sender)query.filter('sender', '=', req.query.sender);
    const [data,metaInfo] = await datastore.runQuery(query);
    res.status(200);
    res.send(data);
});

app.get("/messages/:mid", async(req, res) => {
    const mid = Number(req.params.mid);
    const key = datastore.key(['Message', mid])
    const result = await datastore.get(key);
    if(result[0]){
        res.send(result[0]);
    }else{
        res.status(404);
        res.send();
    }
});

app.post("/messages", async (req, res) => {
    const message = req.body;
    message.timestamp = Date.now();
    try{
        const sResult = await axios.get(`${loginURL}/users/${message.sender}/verify`);
        const rResult = await axios.get(`${loginURL}/users/${message.recipient}/verify`)
        const key = datastore.key(['Message'])
        const response = await datastore.save({key:key,data:message})
        res.status(201);
        res.send(response);

    }catch(error){
        res.status(404);
        res.send("Invalid Message!")
    }
    
});